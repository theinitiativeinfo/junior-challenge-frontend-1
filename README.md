# Junior Challenge FRONTEND 1

## Login Credentials for your project :
You will find these in your personal REPO :)

## About
This is a little electricity readings panel app that should be done with the use of AngularJS 1.x and serves as a little frontend skills test. Its intention is to get juniors started.

## Guidelines
The main parts would be:

### - LOGIN (tipical login view)
### - - LIST READINGS (table or something that lists readings and reading dates)
### - - ADD READING (view to submit a new reading)
### - - UPDATE READING (view/popup/inline to update a reading) (optional)
### - - DELETE READING (button to delete a reading on LIST READINGS) (optional)


For those who had no connections with angular until now here are a few resources to get you started:

* `https://www.w3schools.com/angular/`
* `https://code.tutsplus.com/tutorials/building-a-web-app-from-scratch-in-angularjs--net-32944`
* `https://www.codeschool.com/courses/shaping-up-with-angularjs`
* `https://www.codeschool.com/courses/staying-sharp-with-angularjs`
* `aaand google :)`

Also feel free to use a framework like Angular Material or if you know Bootstrap combine it with Angular UI Bootstrap:

* `https://material.angularjs.org/latest/`
* `https://angular-ui.github.io/bootstrap/`


## Backend
The app comunicates with the backend using REST calls.
Structure:

* Login
* List Readings
* Add Reading
* Update a Reading (Optional)
* Delete a Reading (Optional)
  
### Login
Method: 
__POST__

URL:
`https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyDQCPz2IE4dbvhPRqF5y1vVmJB2J9sGQRM`

Params:
```
{
  email: 'user@user.ro',
  password: 'userPassword',
  returnSecureToken: true
}
```    

Example Response:
```
{
 "kind": "identitytoolkit#VerifyPasswordResponse",
 "localId": "LGStuBol2hQLG5rKVT5CPzBtnc83",
 "email": "user@user.ro",
 "displayName": "",
 "idToken": "eyJhbGciOiJSUzI1NiIsImtpZCI6IjI1Y2E3OTFmNmYxYjRlOTJhYzljNDRhOTc4M2E0MDdiNzgyMmNiMDAifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vZnJvbnRlbmQtdGVzdC1wcm9qZWN0LTEtYWJ2IiwiYXVkIjoiZnJvbnRlbmQtdGVzdC1wcm9qZWN0LTEtYWJ2IiwiYXV0aF90aW1lIjoxNTA5MDk5ODE3LCJ1c2VyX2lkIjoiTEdTdHVCb2wyaFFMRzVyS1ZUNUNQekJ0bmM4MyIsInN1YiI6IkxHU3R1Qm9sMmhRTEc1cktWVDVDUHpCdG5jODMiLCJpYXQiOjE1MDkwOTk4MTcsImV4cCI6MTUwOTEwMzQxNywiZW1haWwiOiJ1c2VyQHVzZXIucm8iLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsiZW1haWwiOlsidXNlckB1c2VyLnJvIl19LCJzaWduX2luX3Byb3ZpZGVyIjoicGFzc3dvcmQifX0.EPKBMuOnOCp4QjE8fcFEvulohOi3VsJ4bm5osvRpq6Nb8JugfH-xUVnR6qyRfCGcKyO39WZwVnd9oGNJNrbcwsnPQEZ3cPM4w8OcNjcSvQko695l4v1L7_-Y98s0WnmTRJWN4hPRiVdSkUXFaegIx9tV00kUPjjKZVgxJhhSCuaM32hxZ4-vZQM1Yw9WKR0R-NITIOvRnQW6gd_56gGgSY8gBDV9AZhi63qWjfXvFChjlbBr7_8P6UlKYZPrTzENA5ZJXpe4_zrW_4AdI7_Q2CcutOcq6qGn-_4cIXxzArda7w6rKkmBtAHhX29rcEMrmqDYAaLL6K4gl9_NhNZ_XA",
 "registered": true,
 "refreshToken": "APWA_kpZAeKvzZ3v2GyfyRp-mgRMFCPaF8NDz5_A07GXMRR-J1sMydViLk4JPdw_geL5iQHarRDNycW3H6CV74k5uc4VA0fcxILdFdY2b9qcBeT1U7TPjO87IbsI583ZbFTwLgctUHKK4iex_w324RPX0dJnROkMc22oM8Kk-kpsLH3gjwu5jVaGeE2BZGeK9DMU7nWuYdqGgZIWrbQYhd5vGYv-g58SKidV0Gq_M15QoX4IdINtQXM",
 "expiresIn": "3600"
}
```
> __localId__ and __idToken__ will be used for further backend calls


### List Readings
Method: 
__GET__

URL:
`'https://frontend-test-project-1-abv.firebaseio.com/readings/' + (the localId returned at login) + '.json?auth=' + (the idToken returned at login)`

```https://frontend-test-project-1-abv.firebaseio.com/readings/LGStuBol2hQLG5rKVT5CPzBtnc83.json?auth=eyJhbGciOiJSUzI1NiIsImtpZCI6IjI1Y2E3OTFmNmYxYjRlOTJhYzljNDRhOTc4M2E0MDdiNzgyMmNiMDAifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vZnJvbnRlbmQtdGVzdC1wcm9qZWN0LTEtYWJ2IiwiYXVkIjoiZnJvbnRlbmQtdGVzdC1wcm9qZWN0LTEtYWJ2IiwiYXV0aF90aW1lIjoxNTA5MDk5ODE3LCJ1c2VyX2lkIjoiTEdTdHVCb2wyaFFMRzVyS1ZUNUNQekJ0bmM4MyIsInN1YiI6IkxHU3R1Qm9sMmhRTEc1cktWVDVDUHpCdG5jODMiLCJpYXQiOjE1MDkwOTk4MTcsImV4cCI6MTUwOTEwMzQxNywiZW1haWwiOiJ1c2VyQHVzZXIucm8iLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsiZW1haWwiOlsidXNlckB1c2VyLnJvIl19LCJzaWduX2luX3Byb3ZpZGVyIjoicGFzc3dvcmQifX0.EPKBMuOnOCp4QjE8fcFEvulohOi3VsJ4bm5osvRpq6Nb8JugfH-xUVnR6qyRfCGcKyO39WZwVnd9oGNJNrbcwsnPQEZ3cPM4w8OcNjcSvQko695l4v1L7_-Y98s0WnmTRJWN4hPRiVdSkUXFaegIx9tV00kUPjjKZVgxJhhSCuaM32hxZ4-vZQM1Yw9WKR0R-NITIOvRnQW6gd_56gGgSY8gBDV9AZhi63qWjfXvFChjlbBr7_8P6UlKYZPrTzENA5ZJXpe4_zrW_4AdI7_Q2CcutOcq6qGn-_4cIXxzArda7w6rKkmBtAHhX29rcEMrmqDYAaLL6K4gl9_NhNZ_XA```


Example Response:
```
{
   "-KxSQ4a3r1wj4j6E7ill":{
      "date":"2017-10-27T10:41:42.403Z",
      "value":100
   },
   "-KxSQ9oONauPzcG9Q0Db":{
      "date":"2017-10-27T10:42:03.804Z",
      "value":200
   }
}
```


### Add Reading
Method: 
__POST__

URL:
`'https://frontend-test-project-1-abv.firebaseio.com/readings/' + (the localId returned at login) + '.json?auth=' + (the idToken returned at login)`

```https://frontend-test-project-1-abv.firebaseio.com/readings/LGStuBol2hQLG5rKVT5CPzBtnc83.json?auth=eyJhbGciOiJSUzI1NiIsImtpZCI6IjI1Y2E3OTFmNmYxYjRlOTJhYzljNDRhOTc4M2E0MDdiNzgyMmNiMDAifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vZnJvbnRlbmQtdGVzdC1wcm9qZWN0LTEtYWJ2IiwiYXVkIjoiZnJvbnRlbmQtdGVzdC1wcm9qZWN0LTEtYWJ2IiwiYXV0aF90aW1lIjoxNTA5MDk5ODE3LCJ1c2VyX2lkIjoiTEdTdHVCb2wyaFFMRzVyS1ZUNUNQekJ0bmM4MyIsInN1YiI6IkxHU3R1Qm9sMmhRTEc1cktWVDVDUHpCdG5jODMiLCJpYXQiOjE1MDkwOTk4MTcsImV4cCI6MTUwOTEwMzQxNywiZW1haWwiOiJ1c2VyQHVzZXIucm8iLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsiZW1haWwiOlsidXNlckB1c2VyLnJvIl19LCJzaWduX2luX3Byb3ZpZGVyIjoicGFzc3dvcmQifX0.EPKBMuOnOCp4QjE8fcFEvulohOi3VsJ4bm5osvRpq6Nb8JugfH-xUVnR6qyRfCGcKyO39WZwVnd9oGNJNrbcwsnPQEZ3cPM4w8OcNjcSvQko695l4v1L7_-Y98s0WnmTRJWN4hPRiVdSkUXFaegIx9tV00kUPjjKZVgxJhhSCuaM32hxZ4-vZQM1Yw9WKR0R-NITIOvRnQW6gd_56gGgSY8gBDV9AZhi63qWjfXvFChjlbBr7_8P6UlKYZPrTzENA5ZJXpe4_zrW_4AdI7_Q2CcutOcq6qGn-_4cIXxzArda7w6rKkmBtAHhX29rcEMrmqDYAaLL6K4gl9_NhNZ_XA```

Params:
```
{
date: 'date here',
value: 'reading value here'
}
```   

Example Response:
```
{"name":"-KxSYIFsJH8aDkSFb7xm"}
```


### Update a Reading (Optional)
Method: 
__PUT__

URL:
`'https://frontend-test-project-1-abv.firebaseio.com/readings/' + (the localId returned at login) + '/' + (the object name of the reading entry) + '.json?auth=' + (the idToken returned at login)`

```https://frontend-test-project-1-abv.firebaseio.com/readings/LGStuBol2hQLG5rKVT5CPzBtnc83/-KxSQ9oONauPzcG9Q0Db.json?auth=eyJhbGciOiJSUzI1NiIsImtpZCI6IjI1Y2E3OTFmNmYxYjRlOTJhYzljNDRhOTc4M2E0MDdiNzgyMmNiMDAifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vZnJvbnRlbmQtdGVzdC1wcm9qZWN0LTEtYWJ2IiwiYXVkIjoiZnJvbnRlbmQtdGVzdC1wcm9qZWN0LTEtYWJ2IiwiYXV0aF90aW1lIjoxNTA5MDk5ODE3LCJ1c2VyX2lkIjoiTEdTdHVCb2wyaFFMRzVyS1ZUNUNQekJ0bmM4MyIsInN1YiI6IkxHU3R1Qm9sMmhRTEc1cktWVDVDUHpCdG5jODMiLCJpYXQiOjE1MDkwOTk4MTcsImV4cCI6MTUwOTEwMzQxNywiZW1haWwiOiJ1c2VyQHVzZXIucm8iLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsiZW1haWwiOlsidXNlckB1c2VyLnJvIl19LCJzaWduX2luX3Byb3ZpZGVyIjoicGFzc3dvcmQifX0.EPKBMuOnOCp4QjE8fcFEvulohOi3VsJ4bm5osvRpq6Nb8JugfH-xUVnR6qyRfCGcKyO39WZwVnd9oGNJNrbcwsnPQEZ3cPM4w8OcNjcSvQko695l4v1L7_-Y98s0WnmTRJWN4hPRiVdSkUXFaegIx9tV00kUPjjKZVgxJhhSCuaM32hxZ4-vZQM1Yw9WKR0R-NITIOvRnQW6gd_56gGgSY8gBDV9AZhi63qWjfXvFChjlbBr7_8P6UlKYZPrTzENA5ZJXpe4_zrW_4AdI7_Q2CcutOcq6qGn-_4cIXxzArda7w6rKkmBtAHhX29rcEMrmqDYAaLL6K4gl9_NhNZ_XA```

Params:
```
{
date: 'date here',
value: 'reading value here'
}
```   

Example Response:
```
{
date: 'date here',
value: 'reading value here'
}
```


### Delete a Reading (Optional)
Method: 
__DELETE__

URL:
`'https://frontend-test-project-1-abv.firebaseio.com/readings/' + (the localId returned at login) + '/' + (the object name of the reading entry) + '.json?auth=' + (the idToken returned at login)`

```https://frontend-test-project-1-abv.firebaseio.com/readings/LGStuBol2hQLG5rKVT5CPzBtnc83/-KxSQ9oONauPzcG9Q0Db.json?auth=eyJhbGciOiJSUzI1NiIsImtpZCI6IjI1Y2E3OTFmNmYxYjRlOTJhYzljNDRhOTc4M2E0MDdiNzgyMmNiMDAifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vZnJvbnRlbmQtdGVzdC1wcm9qZWN0LTEtYWJ2IiwiYXVkIjoiZnJvbnRlbmQtdGVzdC1wcm9qZWN0LTEtYWJ2IiwiYXV0aF90aW1lIjoxNTA5MDk5ODE3LCJ1c2VyX2lkIjoiTEdTdHVCb2wyaFFMRzVyS1ZUNUNQekJ0bmM4MyIsInN1YiI6IkxHU3R1Qm9sMmhRTEc1cktWVDVDUHpCdG5jODMiLCJpYXQiOjE1MDkwOTk4MTcsImV4cCI6MTUwOTEwMzQxNywiZW1haWwiOiJ1c2VyQHVzZXIucm8iLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsiZW1haWwiOlsidXNlckB1c2VyLnJvIl19LCJzaWduX2luX3Byb3ZpZGVyIjoicGFzc3dvcmQifX0.EPKBMuOnOCp4QjE8fcFEvulohOi3VsJ4bm5osvRpq6Nb8JugfH-xUVnR6qyRfCGcKyO39WZwVnd9oGNJNrbcwsnPQEZ3cPM4w8OcNjcSvQko695l4v1L7_-Y98s0WnmTRJWN4hPRiVdSkUXFaegIx9tV00kUPjjKZVgxJhhSCuaM32hxZ4-vZQM1Yw9WKR0R-NITIOvRnQW6gd_56gGgSY8gBDV9AZhi63qWjfXvFChjlbBr7_8P6UlKYZPrTzENA5ZJXpe4_zrW_4AdI7_Q2CcutOcq6qGn-_4cIXxzArda7w6rKkmBtAHhX29rcEMrmqDYAaLL6K4gl9_NhNZ_XA```
 
 
##
Backend Test

### Use a tool like Postman (https://www.getpostman.com/)

